import React from "react";
import ToDoItems from './ToDoItems';
import './ToDoList.css';

class ToDoList extends React.Component{

    constructor(props){
        super(props);
        this.state = {items: []};
        
        this.addItem = this.addItem.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
    }

    addItem(e){
        if (this.input.value !== ""){
            var newItem ={
                text: this.input.value,
                key: Date.now()
            };

            this.setState((prevState) => {
                return {
                    items: prevState.items.concat(newItem)
                };
            });

            this.input.value = "";

        }

        console.log(this.state.items);

        e.preventDefault();
    }

    deleteItem(key){
      var filteredItems = this.state.items.filter(function (item) {
        return (item.key !== key);
      });

      this.setState({
        items: filteredItems
      });
    }

  render(){
    return(
      <div className="todoListMain">

        <div className="header">
          <h1 style={{textAlign: 'center'}}>My To Do List</h1>
          <h3 style={{textAlign: 'center'}}>Write your todos below</h3>
          <form onSubmit={this.addItem}>
            <input ref={(a) => this.input = a} placeholder="Enter Task"></input>
            <button type="submit">Add</button>
          </form>
        </div>
        <ToDoItems 
          entries={this.state.items} 
          delete={this.deleteItem} />
          <h6 style={{textAlign: 'center'}}>*To delete, just click on the to do you want to delete</h6>
      </div>
    )
  }
}

export default ToDoList;


